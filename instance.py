class A():
    def __init__(self, attr):
        self.attr = attr
    def setAttr(self, attr):
        self.attr = attr
    def getAttr(self):
        print(self.attr)

a = A('a')
# A클래스의 a인스턴스
b = A('b')
# A클래스의 b인스턴스
c = A('c')
# A클래스의 c인스턴스

a.setAttr('aaa')
# a에 저장된 A('a')를 A('aaa')로 저장
a.getAttr()
# a에 저장된 클래스의 인스턴스 출력
A('a').getAttr()
b.getAttr()
c.getAttr()