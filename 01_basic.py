# for 반복문
#    특정 작업을 위해 정해진 횟수 만큼 반복 실행을 하기위해 사용

# 실습
    # >>> range(5)
    # range(0, 5)
    # >>> list(range(5))
    # [0, 1, 2, 3, 4]

반복횟수 = range(5)    ## range: 반복할 수 있는 구조로 만듬, range(5) 5번 반복
for 변수명 in 반복횟수:
    print(변수명, '반복 작업')

    # 0 반복 작업
    # 1 반복 작업
    # 2 반복 작업
    # 3 반복 작업
    # 4 반복 작업

for item in [0, 1, 2, 3, 4]:    ## list 를 사용해서 반복
    print(item, '번째 반복')

    # 0 번째 반복
    # 1 번째 반복
    # 2 번째 반복
    # 3 번째 반복
    # 4 번째 반복

items = ['python3', 'python3-pip', 'python3-devel', 'mariadb', 'mariadb-devel']
print('--반복문--')
for item in items:
    print('yum -y install', item)

    # --반복문--
    # yum -y install python3
    # yum -y install python3-pip
    # yum -y install python3-devel
    # yum -y install mariadb
    # yum -y install mariadb-devel

print('--반복문/조건문--')
for item in items:    ## if 문 사용해서 items에서 python3 가 포함된  내용 출력
    if 'python3' in item:
        print('yum -y install', item)

    # --반복문/조건문--
    # yum -y install python3
    # yum -y install python3-pip
    # yum -y install python3-devel

dict_items = {
    'django': '2.2.14',
    'mysqlclient': '1.4.16'
}

for key, item in dict_items.items():    ## dict_items 변수에 쓸수 있는 함수 중에 dict_items.items 가 있다, 사전형 집합 자료형으로 변환
    print(f'pip3 install {key}=={item}')
## f : 출력할 형식을 지정할 때 사용, printf랑 비슷
## unpacking: django, mysqlclient 가 0번째로, 2.2.14, 1.4.16 이 1번째로 
## [('django', '2,2,14'), ('mysqlclient', '1.4.16')]

services = [
    {'name': "firewalld", 'state': "stopped", 'enabled': "no"},
    {'name': "httpd", 'state': "started", 'enabled': "yes"},
    {'name': "chronyd", 'state': "started", 'enabled': "yes"}
]

# 3.5 .format
# 3.6 f' '
for item in services:
    if item['state'] == 'stopped':
        print(f'systemctl stop {item["name"]}')
    elif item['state'] == 'started':
        print(f'systemctl start {item["name"]}')
    elif item['state']  == 'restarted':
        print(f'systemctl restart {item["name"]}')
    if item['enabled'] == 'yes':
        print(f'systemctl enable {item["name"]}')
    else:
        print(f'systemctl disbled {item["name"]}')

    # systemctl stop firewalld
    # systemctl disbled firewalld
    # systemctl start httpd
    # systemctl enable httpd
    # systemctl start chronyd
    # systemctl enable chronyd

# for 반복문은 정해진 횟수, 특정 배열(반복가능자료형-iterator)에 존재하는 
# 요소를 사용하여 반복
for x in range(10):
    pass

# while 반복문은 정해진 횟수, 무한 반복이 필요한 경우, 조건에 따라 반복을 해야하는 경우
# while True:    ## 조건식이 True면 무한 반복
#    pass

'''
x = 0
while x < 5:
    print(x)
    x = x + 1
'''

'''
x = 0
while True:
    print(x)
    x = x + 1
    continue    ## 반복문에서 아래 코드를 생략하고 다음 반복을 실행
    if x >= 5:    ## x == 5 보다 무한 반복 방지에 대해 좀 더 안정적
        break
'''

# 반복문에서 사용하는 else 구문
# else는 break를 통해 강제 종료를 하지 않았을 경우에 동작을 한다
# 반복이 종료되고 수행할 코드가 있을 때 
# break를 통해 강제 종료가 안된 경우에만 수행할 코드가 있다면 else를 활용하는게 편하다
'''
for x in range(10):
    pass
else:
    pass

x = 0
while x < 5:
    pass
else:
    pass
'''

