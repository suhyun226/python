id_index = 1
users = [
    {'id': 1, 'name': 'user1', 'pass': 'user1pass', 'perm': 6},
    {'id': 2, 'name': 'user2', 'pass': 'user2pass', 'perm': 4},
    {'id': 3, 'name': 'user3', 'pass': 'user3pass', 'perm': 4},
    {'id': 4, 'name': 'user4', 'pass': 'user4pass', 'perm': 7},
    {'id': 5, 'name': 'user5', 'pass': 'user5pass', 'perm': 6},
    {'id': 6, 'name': 'user6', 'pass': 'user6pass', 'perm': 1},
    {'id': 7, 'name': 'user7', 'pass': 'user7pass', 'perm': 2},
    {'id': 8, 'name': 'user8', 'pass': 'user8pass', 'perm': 3},
    {'id': 9, 'name': 'user9', 'pass': 'user9pass', 'perm': 5}
]
'''
# perm은 권한으로 rwx의 권한을 숫자로 표현을 한 것이다
# 6 -> rw- -> 110
# 4 -> r-- -> 100
# 7 -> rwx -> 111
# 2 -> -w- -> 010

# 읽기, 쓰기 권한을 가지는 사용자만 출력 하시오
print('1. 읽기 쓰기 권한을 가지는 사용자 출력하기.')
for user in users:
    if user.get('perm') == 6:
        print(user.get('name'), user['name'])
        # get 이랑 [] 모두 출력 결과가 비슷하지만 만약 name 이라는 키가 없으면 error 뜬다
        # get은 error 안뜨고 none 이 나옴
    elif user.get('perm') == 4:
        print(user.get('name'))
    elif user.get('perm') == 2:
        print(user.get('name'))

print('\n')

# 사용자로 부터 입력을 받을 때 사용
# input
# while True:
#     chk_perm = int(input('0~7 까지의 값 입력 : '))
#     if 0 <= chk_perm <= 7:
#         chk_perm = tuple(f'{chk_perm:03b}')
#         # 03b <- 3자리수를 유지한채 2진수 값으로 변환
#         # 요소 하나씩 tuple 의 요소로 만듬
#         break
#     print('잘못된 입력 값입니다.')

# print(chk_perm)

# for user in users:
#     if user.get('perm') in (6, 4, 2):
#         # in 사용
#         print(user.get('name'))
#         print(bin(user.get('perm')))
#         print(f'{6:b}', f'{user.get("perm"):b}')
        # 이진수 변환
        # bin()
        # :b <- 이진수 서식 사용

# 0b <- 이진수를 뜻함

while True:
    chk_perm = 'r'
    # chk_perm = input('읽기(r), 쓰기(w), 실행(x) 중 하나 입력 : ')
    tmp = {'r': 4, 'w': 2, 'x': 1}
    if tmp.get(chk_perm, False):
        # r,w,x 이외의 값 입력 했을시 None이 나오는 경우 False로 정의하고 중지
        # print 잘못된 값
        chk_perm = tmp.get(chk_perm)
        break
    print('잘못된 입력 값입니다.')

for user in users:
    if user.get('perm') & chk_perm == chk_perm:
        print(user.get('name'), user.get('perm'))

print('\n')


# 새로운 사용자를 추가 하는 코드를 작성

print('2. 새로운 사용자 추가')

while True:
    name = input('추가 할 사용자 명 : ')
    if name == '':
        print('이름을 입력하세요.')
        continue
    for user in users:
        if user.get('name') == name:
            print('해당 계정이 이미 존재 합니다.')
            break
    else:
        break

while True:
    password = input('패스워드 : ')
    if password == '':
        print('패스워드를 입력하세요.')
        continue
    break


while True:
    perm = input('권한(rwx) 형식으로 입력 : ')
    if perm == '':
        print('권한을 설정하지 않았습니다.')
        continue

    if len(perm) > 3:
        print('잘못된 형식 입니다.')

    perm_chk = 0
    for p in perm:
        if p not in 'rwx':
            perm_chk += 1

    if perm_chk > 0:
        print('잘못된 권한 코드가 있습니다. 다시 입력하세요')
        continue
    break

perm_code = 0
for p in perm:
    if p == 'r':
        perm_code += 4
    elif p == 'w':
        perm_code += 2
    elif p == 'x':
        perm_code += 1


users.append({})
users[-1].setdefault('id', id_index)
users[-1].setdefault('name', name)
users[-1].setdefault('pass', password)
users[-1].setdefault('perm', perm_code)
# -1 <- 맨 마지막 요소 지정

# global id_index
# id_index += 1
# users.append(
#     {'id': id_index + 1, 'name': name, 'pass': password, 'perm': perm_code}
# )

'''

# 기존 사용자를 삭제 하는 코드 작성

print('3. 기존 사용자를 삭제.')

while True:
    name = input('삭제 할 사용자 명 : ')
    if name == '':
        print('이름을 입력하세요.')
        continue

    for user in users:
        if user.get('name') == name:
            users.remove(user)
            break
    else:
        print('해당하는 사용자가 존재하지 않습니다.')
        continue
    break

print(users)

# 기존 사용자의 권한을 수정하는 코드 작성



print('\n' * 20)
