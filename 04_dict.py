dct = {}
print(dir(dct))

                 # key : value
# immutation(변경불가) : mutation(변경가능)


# dct.clear
# dct.copy

dct = dict.fromkeys(['a', 'b', 'c', 'd'])
print(dct)

dct = dict.fromkeys(['a', 'b', 'c', 'd'], 'Default')
print(dct)

# 키를 사용해서 사전형 자료 만든다


dct.setdefault('e')
print(dct)

dct.setdefault('f', 10)
print(dct)

dct.setdefault('f', 20)    ## 이미 동일한 키가 있으면 setdefault로 수정 안됨 ->> update 사용
print(dct)

# 새로운 키:값 을 추가


dct.update({'f': 30})
print(dct)

dct.update({'g': 40})
print(dct)

dct.update({'g': 50, 'h': 60})
print(dct)

# 있는 거면 수정, 없는 거면 추가
# update가 setdefault에 비해 좀더 범용적이다

dct.update([('a', 1), ('b', 2), ('c', 3)])
print(dct)

# 리스트 형식으로도 수정 된다


item = dct.popitem()
print(dct, item)

# 마지막 데이터가 빠진다
# 3.5버전은 처음의 데이터가 빠진다
# 키와 값이 반환 된다


value = dct.pop('d')
print(dct, value)

# 해당 키와 값이 빠진다
# 값만 반환 된다

value = dct.pop('k', 'no data')
print(dct, value)

# k 라는 키가 없으면, 오른쪽의 값 반환
# 오른쪽의 값이 없으면 error


print(dct.get('a'))

print(dct.get('k', 'NoData'))

# 해당 키의 값 반환, 없으면 오른쪽의 값 반환


### items, keys, values 는 리스트 형식으로 보이기 때문에 반복문에서 활용 가능하다 ###

print(dct.items())
for item in dct.items():
    print(item)

# 키값 쌍으로 반환


print(dct.keys())
for key in dct.keys():
    print(key)

# 키만 반환


print(dct.values())
for value in dct.values():
    print(value)

# 값만 반환



print('\n' * 20)