# 함수 : 프로그램을 기능 단위로 묶어서 사용할 수 있게 한다.
#        함수는 선언, 호출, 반환의 작업을 통해 동작이 이루어 진다

# 선언 : 함수를 정의하는 것.
#        def 구문을 사용하여 선언 한다.

# 호출 : 선언한 함수를 실행하는 것.
#        함수를 실행할 때에는 반드시 함수명() 형태로 실행한다
#        함수명 뒤에 ()를 사용하지 않으면, 실해되지 않는다.
#        () 안에는 함수에 전달할 매개변수(인자)를 넣을 수 있다.

# 반환 : 실행된 함수의 결과를 돌려주는 것.
#        기본적으로 반환은 None을 돌려준다.
#        return 구문을 사용하여 반환할 데이터를 정의한다.
#        return 은 오직 하나의 데이터만 반환한다.

# 선언
def 함수명():
    pass

# 호출
함수명()

# 반환 값 변수에 저장
ret = 함수명()
print(ret)

# None

print('\n')

def 함수명():
    return 1

함수명()

ret = 함수명()
print(type(함수명), ret, 함수명)
# <class 'function'> 1 <function 함수명 at 0x0000022C3105CA60>
# 함수명에 ()를 안하면 함수의 주소를 나타냄

print('\n')


# 매개변수가 있는 함수
def func1(x, y):
    z = x % y
    return z

# ret = func1()
# TypeError: func1() missing 2 required positional arguments: 'x' and 'y'    
# 두개의 매개변수를 요구
# 인자의 위치 중요

ret = func1(5, 3)
print(ret)
# 2

# 매개변수 명을 지정하여 함수 사용
ret = func1(y=5, x=3)
print(ret)
# 3

# 가변 매개변수를 사용하는 함수
def func2(*args):
    # *이 가변 매개변수 만들때 사용
    # args는 arguments의 약자
    res = 0
    for x in args:
        res += x
    return res, args
    # 값을 두개로 반환해도 튜플로 알아서 packing하여 하나로 반환된다

ret = func2()
print(ret)

ret = func2(10)
print(ret)

ret = func2(10, 20)
print(ret)

ret = func2(10, 20, 30)
print(ret)
# 가변이기 때문에 매개변수를 몇개를 넣든 상관없다
# (0, ())
# (10, (10,))
# (30, (10, 20))
# (60, (10, 20, 30))

print('\n')


# 기본값을 사용하는 매개변수
def func3(x, y, z=0):
    res = x + y
    if z:
        res = res / z
    return res

ret = func3(10,20)
print(ret)
# 30

ret = func3(10,20,2)
print(ret)
# 15.0
# 0으로 나누기가 않될때 사용

def func4(num, *args, z=0):
    for x in args:
        num += x
    if z:
        num = num / z
    return num

ret = func4(1, 2, 3, 4, 5, 6, 7, 8, 9, z=5)
print(ret)
# 9.0

print('\n')


# 가변 키워드 매개변수
def func5(**kwargs):
    s = ''
    num = 1
    for k, v in kwargs.items():
        s += f'{num} - {k}: {v}\n'
        num += 1
    return s, kwargs

ret = func5(a=1, b=2, c=3)
# 매개변수를 함수에서 사전형(.items(키,값 쌍))으로 활용
# [('a',1), ('b',2), ('c',3)]
print(ret[0], ret[1])
# 1 - a: 1
# 2 - b: 2
# 3 - c: 3
#  {'a': 1, 'b': 2, 'c': 3}



# 모든 매개변수 활용
def func6(x, y, *args, z=0, **kwargs):
# 매개변수 활용 순서
    return x, y, args, z, kwargs

ret = func6(1, 2, 4, 5, 6, 7, z=0, name='a', age=20)
print(ret)
# (1, 2, (4, 5, 6, 7), 0, {'name': 'a', 'age': 20})


print('\n' * 20) 