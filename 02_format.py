### % 식을 사용한 방법 <- python2 버전 ###

string = '정수 1개 출력 : %d' % 10
print(string)

string = '정수 2개 출력 : %d, %d' % (10,20)
print(string)

string = '정수 3개 출력 : %d, %d, %d' % (10,20,30)
print(string)

string = '문자열 출력 : %s' % 'abcd'
print(string)

string = '실수 출력 : %.2f' % 1.1234
print(string)

### .format() 문자열 함수를 사용한 방법 <- python3.5 버전 ###

string = '정수 1개 출력 : {:d}'.format(10)
print(string)

string = '정수 2개 출력 : {:d}, {:d}'.format(10, 20)
print(string)

string = '정수, 실수, 문자열 출력 : {}, {}, {}'\
    .format(10, 2.2, 'abcdㅁ')
print(string)

string = '소수점 3자리 실수 : {:.3f}'.format(1.1234)
print(string)

string = '인덱스 값 활용 : {1:d} {2:d} {0:d}'\
    .format(10, 20, 30)    ## : 앞에 배열의 위치를 지정하여 출력 값의 순서를 바꿀 수 있다
print(string)

string = '인덱스 값 활용 : {0:d} {0:d} {0:d}'\
    .format(10)    ## 0번 인덱싱을 하지 않으면 튜플 범위에서 벗어난다고 뜬다 error
print(string)

'''
{인덱스:포멧}
인덱스 생략하면 0번
포맷 생략하면 알아서 지정
'''

lst = [100, 200, 300, 400]
string = '리스트의 요소 활용 : {}'.format(lst[-3:])
print(string)

#       0    1    2    3   4
# lst = [100, 200, 300, 400]
#      -4   -3   -2   -1   
## 자를 생각하고 계산 하면 된다

# lst[1:3]
 # [200 300]
# lst[:-2] -> lst[0:-2] 0에서 -2까지
 # [100 200] 
# lst[-3:]
 # [200 300 400]
# lst[시작 인덱스:끝 인덱스:건너띄기?]
# 건너띄기는 기본 1로 한칸 씩 이동, 2면 하나 건너 띄고 그 다음으로 이동
# lst[::] -> 전체
# lst[0:4:1] -> lst[::]
 # 1 (양수)이면 왼쪽에서 부터 인덱싱
# lst[4:0:-1] -> lst[::-1]
 # -1 (음수)이면 오른쪽에서 부터 인덱싱

string = '리스트의 요소 활용 : {1}'.format(*lst)
print(string)

# packing 되어 있는 리스트를 * 로 unpacking 해서 리스트의 특정 요소를 지정할 수 있다

lst = [1,2,3,4,5,6,7]
string = '정방향 출력 : {}'.format(lst)
print(string)

rlst = lst[::-1]
string = '역방향 출력 : {}'.format(rlst)
print(string)

lst = ['a', 'h', 'r', 'y', 'j', 's']
string = '리스트 출력 : {}'.format(lst)
print(string)

asc_lst = sorted(lst)    ## sorted : 정렬 시키는 내장 함수, 기존 lst의 순서를 건들지는 않는다
string = '오름차순 출력 : {}'.format(asc_lst)
print(string)

des_lst = sorted(lst, reverse=True)
string = '내림차순 출력 : {}'.format(des_lst)
print(string)

lst.sort()    ## .sort() : lst 자체의 순서를 바꿈
string = '오름차순으로 정렬된 변수 출력 : {}'.format(lst)
print(string)

lst.sort(reverse=True)
string = '내람차순으로 정렬된 변수 출력 : {}'.format(lst)
print(string)

print(sorted(lst))
print(lst.sort())    ## 반환값이 없으면 None 출력

dct = {'키': '값', 'x': 10, 'y': 20, 'z': 30}    ## 키는 문자열이어야 좋다
string = '사전형 출력 : {}'.format(dct)
print(string)

# 키를 확인하기 때문에 순서와는 상관없다
# 3.5에서는 순서가 다르게 나오지만 상관할 필욘 없다

string = '사전형 출력 : {키}, {x}'.format(**dct)
print(string)

# 사전형은 **가 unpacking
# unpacking 후 키{x}를 적어서 해당 키의 값을 찾는다

lst1 = [1,2,3,4,5]
lst2 = ['a', 'b', 'c', 'd', 'e']

x = list(zip(lst1, lst2))    ## 피벗
print('\n')
print(x)
print('\n')

string = '이중 리스트 출력 : {3}'.format(*x)
print(string)


x = dict(zip(lst2, lst1))    ## 피벗
print(x)

string = '사전형 이중 리스트 출력 : {b}'.format(**x)
print(string)


### f'' 을 활용한 방법 3.6버전 부터 가능 ###

# format-string

x = 10
string = f'정수 : {x}'
print(string)

x, y, z = 65, 'a', 65.0
string = f'값 출력: {x} {y} {z:.2f}'
print(string)

lst = [1,2,3,4]
string = f'리스트 출력 : {lst}'
print(string)

string = f'리스트 출력(슬라이싱) : {lst[1:3]}'
print(string)

string = f'리스트 출력(인덱싱) : {lst[1]}'
print(string)

dct = {'a':1, 'b':2, 'c':3}
string = f'사전 출력 : {dct}'
print(string)

string = f'사전 출력 : {dct["a"]}'    ## 앞에서 '(작은 따옴표)를 썻으면 그 다음은 구분되게 "(큰 따옴표)를 쓴다
print(string)


string = f'{"col1":^8}|{"col2":^8}|{"col3":^8}\n'    ## ^숫자 : 숫자 칸 중 가운데 정렬
string += f'{"-":-<8}+{"-":-<8}+{"-":-<8}\n'    ## 기호<숫자 : 숫자 칸 중 왼쪽 정렬하고 기호를 추가
string += f'{"a":<8}|{"b":<8}|{"c":<8}'    ## <숫자 : 숫자 칸 중 왼쪽 정렬
print(string)


lst = [
    ('Ahn', 32, 'M'),
    ('Park', 30, 'W'),
    ('Kim', 28, 'M')
]

string = f'{"name":^8}|{"age":^8}|{"gender":^8}\n'
string += f'{"-":-<8}+{"-":-<8}+{"-":-<8}\n'
for item in lst:
    string += f'{item[0]:<8}|{item[1]:<8}|{item[2]:<8}\n'
else:
    print(string)


print('\n' * 20)