# subprocess는 shell에서 실행한 결과물을 스크립트로 가져올때 사용
# 기존 스크립트를 실행했을때 shell에 출력되는 것의 반대

import os
import subprocess

print('*' * 50, '시작~하겠습니다~', '*' * 50)

def shellCommand(*args):
    cmd = subprocess.Popen(args, shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
    return cmd.communicate()

print(os.sys.platform)

if os.name == 'nt':
    ret, err = shellCommand('dir')
    print(str(ret, encoding='cp949'))
    print(str(err, encoding='cp949'))
    # 정상 작동하면 ret 출력
    # 에러 발생하면 err 출력
elif os.name == 'posix':
    ret, err = shellCommand('ls')
    print(str(ret, encoding='utf-8'))
    print(str(err, encoding='utf-8'))

