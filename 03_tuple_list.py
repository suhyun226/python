tup = (1, 2, 3, 4, 2, 2, 2)
print(tup, type(tup))

# 하나의 요소만 있는 튜플 자료형을 만들 때 마지막에 쉼표 꼭 있어야 한다
# 마직막에 쉼표가 없으면 자료형이 int로 된다

print(dir(tup))    ## (tup)에서 사용할 수 있는 메서드 확인
print('요소 수', len(tup))
print('지정한 데이터의 수', tup.count(2))    ## tup의 item 중 2의 개수
print('지정한 데이터의 위치', tup.index(4))    ## tup의 item 중 4의 인덱스 값 (만약 여러개면 맨앞 하나만)

lst = [1,2,3,4,5,6,1,2,3,]
print(lst, type(lst))

print(dir(lst))

lst = []
lst.append('a')
lst.append(1)
lst.append([1,2])
print(lst)

# 하나씩 추가 될 때 마다 뒤로 붙는다

lst.extend(['a', 'b'])
print(lst)

# 리스트 안의 자료들이 하나씩 추가 된다

lst.insert(0, 'c')
print(lst)

# 원하는 위치에 자료를 추가한다

lst.remove('a')
# lst.remove([1,2])
lst[2].remove(1)    ## 리스트 안의 리스트 안의 item 지우기
print(lst)

# 지우고 싶은 값 삭제

x = lst.pop()
print(lst, x)

x = lst.pop(2)
print(lst, x)

# 꺼내서 삭제 


# lst.clear()
# print(lst)

# 리스트 내 모든 데이터 삭제

lst.reverse()
print(lst)

# 역순

lst.sort(key=str)
print(lst)

# 리스트 안의 요소들을 문자열을 기준으로 통합후 정렬

lst.sort(key=str, reverse=True)
print(lst)

# 위의 역순

lst = [
    (26, 'Ahn'),
    (28, 'Park'),
    (30, 'Kim')
]
lst.sort(key=lambda x : x[1])    ## lambda : 익명 함수, 함수를 만들지 않고 임시로 쓰기 편하다
print(lst)

# 함수의 x(변수) 인자를 받아서 1번 인덱스 값을 키로 정렬
# ex.. (26, 'Ahn') 변수의 1번 인덱스 Ahn을 기준으로 정렬
# 문자는 아스키코드 참조하여 정렬, A = 65


lst1 = [1,2,3,4]
lst2 = lst1
lst3 = lst1.copy()

lst1.append('a')

print(lst1, lst2, lst3)
print(id(lst1), id(lst2), id(lst3))    ## id : 주소 값
print(hex(id(lst1)), hex(id(lst2)), hex(id(lst3)))

# lst2에 lst1값이 저장된 것이 아니고 lst1의 주소값이 저장되어 있어서 
# lst1에만 데이터 추가해도 lst2에 lst1의 값이 똑같이 나오는 것이다

lst1 = [1,2,3,4]
lst2 = lst1
lst3 = lst1.copy()

print(lst1, lst2, lst3)
print('== 연산', lst1 == lst2, lst1 == lst3)
print('is 연산', lst1 is lst2, lst1 is lst3)

# == 는 값이 같은지 확인
# is 는 주소값이 같은지 확인

print('\n' * 20)