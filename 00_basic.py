## 확장 프로그램: MagicPython, Python 함수 작성에 도움
## print('문자열 작성')    ## python3
## print '문자열 작성'    ## python2

# if 제어문
#   조건식에 따라 코드를 실행하거나 실행하지 않게 제어하기 위한 구문
#   if ... else 또는 if ... elif ... else 형식의 문장 구문을 가진다
#   else 구문은 생략 가능하며, if 문의 조건식이 거짓일 때 실행할 코드가 된다
#   elif 구문은 else if 의 결합 형태이다
#   elif 는 여러번 반복하여 사용할 수 있다

if 1 == 0:
    print('1은 0 이다.')
else:
    print('1은 0 이 아니다.')

char = 'c'
if char == 'a':
    print('문자 a 가 저장되어 있습니다.')    ## 공백(들여쓰기): indent  ->  반드시 스페이스 4칸으로, tab 공백 사용 X (tab 눌렀을때 스페이스 4칸으로 바꿔주는 기능이 있으면 사용)
elif char == 'b':
    print('문자 b 가 저장되어 있습니다.')
elif char == 'c':
    print('문자 c 가 저장되어 있습니다.')
elif char == 'd':
    print('문자 d 가 저장되어 있습니다.')
else:
    print('abcd 중 어떠한 문자도 포함되어 있지 않습니다.')
## 해당 파일 있는 위치로 이동
## python .\00_basic.py  <--  실행

## 에러 찾기
## ctrl + F 에서 찾기: tab 공백, 바꾸기: 스페이스 4칸

## WORD 에서 코드 복붙할때 유의 사항
## '', - 의 표시 형식이 파이선에서 달라질 수 있다
## WORD - 옵션 - 언어교정
## 곧은 따옴표 를 둥근 따옴표로 체크해제
## 하이푼을 대시로 체크해제


# 중첩 if 문

char = 'apple'
# char = ['a', ['b', 'c', 'd'], 'e', 1, 2, 3, 4]    ## len 은 7이 된다
# char = [['a', 'b', 'c', 'd', 'e'], 1, 2, 3, 4]    ## 'a' in char  --> False, 'a' in char[0]  --> True
if 'a' in char:
    if len(char) >= 5:    ## len: length 길이, byte 크기가 아닌 자료수를 본다
        print('a 문자가 포함되어 있는 5자 이상의 단어 입니다.')
    else:
        print('a 문자가 포함되어 있는 5자 미만의 단어 입니다.')
else:
    print('a 문자가 포함되어 있지 않습니다.')


# x == y    ## x, y 가 동일한 값인지 비교
# x > y    ## x, y 중 x가 큰 값인지 비교
# x != y    ## x, y 가 다른 값인지 비교
# x <= y    ## x, y 중 x가 y보다 작거나 같은 값인지 비교
# 실습
    # >>> 1 == 'a'
    # False    ## 비교 가능
    # >>> 1 > 'a'
    # Traceback (most recent call last):
    #     File "<stdin>", line 1, in <module>
    # TypeError: '>' not supported between instances of 'int' and 'str'    ## 비교 불가능
    # >>> str(1) < 'a'
    # True    ## str을 비교 가능하도록 만듬
    # >>> str(1)
    # '1'
    # >>> 1 < ord('a')
    # True
    # >>> ord('a')
    # 97    ## 아스키 코드로 비교
    # >>> str(1234567)    ## 문자열로 변환
    # '1234567'
    # >>> str(1234567.0)
    # '1234567.0'
    # >>> int('123456')    ## 정수형으로 변환
    # 123456
    # >>> '123456' + 5    ## 자료형이 달라서 계산이 안됨
    # Traceback (most recent call last):
    #   File "<stdin>", line 1, in <module>
    # TypeError: must be str, not int
    # >>> '123456' + '5'    ## 자료형끼리는 계산 가능
    # '1234565'
    # >>> '123' * 3    ## '123'을 3번 사용
    # '123123123'
    # >>> float('123456')    ## 실수형으로 변환
    # 123456.0
    # >>> bool('123456')    ## 불형을 변환
    # True
    # >>> bool(0)
    # False
    # >>> bool('') 
    # False
    # >>> list('1234567890')    ## list 자료형으로 변환
    # ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']
    # >>> tuple('1234567890')    ## tuple 자료형으로 변환
    # ('1', '2', '3', '4', '5', '6', '7', '8', '9', '0')
    # >>> x=(1,2,3,4,5)
    # >>> x
    # (1, 2, 3, 4, 5)
    # >>> x[0] =10    ## tuple 은 수정 불가
    # Traceback (most recent call last):
    #   File "<stdin>", line 1, in <module>
    # TypeError: 'tuple' object does not support item assignment
    # >>> x =list(x)    ## list 형으로 변환
    # >>> x
    # [1, 2, 3, 4, 5]
    # >>> x[0] = 10    ## list 는 수정 가능
    # >>> x
    # [10, 2, 3, 4, 5]
    # >>> dict([('a',1), ('b',2), ('c', 3)])    ## 키, 깂 형식 주의
    # {'a': 1, 'b': 2, 'c': 3}
    # >>> dict([('a',1), ('b',(2,3,4)), ('c', 3)]) 
    # {'a': 1, 'b': (2, 3, 4), 'c': 3}