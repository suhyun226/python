id_index = 1
users = [
    {'id': 1, 'name': 'root', 'password': 'root', 'perm': 7, 'home': '/home/root', 'shell': '/bin/bash'},
]

def permCheck(data, octal=True):
    if octal:
        if 0 <= int(data) <= 7:
            return int(data)
        else:
            return -1
    else:
        code_index = ('---', '--x', '-w-', '-wx', 'r--', 'r-x', 'rw-', 'rwx')
        if code_index.count(data) == 0:
            return -1
        return code_index.index(data)

# perm = input('리눅스 권한 형식으로 값을 입력하세요.(rwx 또는 정수) : ')
# res = permCheck(perm, octal=False)
# print(res)

def userExist(username):
    for user in users:
        if user.get('name') == username:
            return users.index(user)

res = userExist('root')
print(res)
# 0
# index는 0부터 시작        

print('\n')

def userAdd(username, password, perm=4, **kwargs):
# 매개변수 순서 주의
    global id_index
    # 전역 변수 호출
    if userExist(username) == None:
        id_index += 1
        user_data = {'id': id_index, 'name': username, 'password': password, 'perm': perm}
        user_data.update(kwargs)
        users.append(user_data)
        return user_data
    else:
        return {}

res = userAdd('user1', 'user1', home='/home/user1', shell='/bin/sh', UID=1100, GID=1100)
print(res)
# {'id': 2, 'name': 'user1', 'pass': 'user1', 'perm': 4, 'home': '/home/user1', 'shell': '/bin/sh', 'UID': 1100, 'GID': 1100}
res2 = userAdd('user2', 'user2')
print(res2)
# {'id': 3, 'name': 'user2', 'pass': 'user2', 'perm': 4} 
# 필수 매개변수 username, password만 입력해도 된다
# user_data의 형식을 따른다

print('\n')

def userUpdate(username, **kwargs):
    if userExist(username) != None: # 일치 여부 판단
        user = users[userExist(username)] 
        for k in user.keys():
    
            # user = {'id': 1, 'name': 'root', 'pass': 'root', 'perm': 7, 'home': '/home/root', 'shell': '/bin/bash'}
            # kwargs = {'pass': 'admin1234', 'home': '/root'}
            # 예시
            user[k] = kwargs.get(k, user[k])
        # users[userExist(username)].update(kwargs)
# userUpdate 함수에는 return 값이 없기 때문에 print 로 출력하면 None 이 뜬다

userUpdate('root', password = 'admin1234', home= '/root')
print(users)
# [
#     {'id': 1, 'name': 'root', 'password': 'admin1234', 'perm': 7, 'home': '/root', 'shell': '/bin/bash'}, 
#     {'id': 2, 'name': 'user1', 'password': 'user1', 'perm': 4, 'home': '/home/user1', 'shell': '/bin/sh', 'UID': 1100, 'GID': 1100}, 
#     {'id': 3, 'name': 'user2', 'password': 'user2', 'perm': 4}
# ]
# root 사용자의 패스워드 홈디렉터리 변경 확인

print('\n')


def userDelete(username):
    if userExist(username) != None:
        users.remove(users[userExist(username)])

userDelete('user2')
print(users)
# [
#     {'id': 1, 'name': 'root', 'password': 'admin1234', 'perm': 7, 'home': '/root', 'shell': '/bin/bash'},
#     {'id': 2, 'name': 'user1', 'password': 'user1', 'perm': 4, 'home': '/home/user1', 'shell': '/bin/sh', 'UID': 1100, 'GID': 1100}
# ]
# user2 사용자 데이터 삭제 확인




print('\n')
