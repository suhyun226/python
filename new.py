# 간단한 for 문장 및 if 문장을 식으로 표현하여 사용
print([x for x in range(10)])
# range(10)을 뒤x에 할당
# 뒤x를 앞x에서 활용

print([chr(x) for x in range(65, 65+26)])

print({chr(x):x for x in range(65, 65+26)})

x = 10
print('5보다 크다' if x > 5 else '5보다 크지 않다')
# if 조건식이 참이면 왼쪽 사용, 거짓이면 오른쪽 사용

x = 3
print('a' if x == 1 else 'b' if x == 2 else 'c' if x == 3 else 'd')
# 마지막 else 생략 불가능