# open 파일 입출력 함수
# 시스템의 모든 파일을 읽고 쓸수 있는 기능을 제공

# 형식
# file = open('파일경로', '읽기/쓰기', encoding='인코딩 종류')

# 인코딩 종류
#   시스템의 기본 인코딩으로 동작
#     - Linux : UTF-8
#     - Windows : CP949

# 동작 모드
#   읽기 모드 : r
#   쓰기 모드 : w(덮어쓰기), a(이어쓰기)
#   바이너리 모드 :  b

rfile = open('D:\\GNS3\\Projects\\LAB_0921\\LAB_0921.gns3', 'r', encoding='utf-8')
# 이스케이프 문자가 있으면 \ 사용
# data = file.read()
# 읽은 문자 data에 저장
# for data in file.readlines():
#     print(data)
# print(file.readlines())
# <class 'list'>
# print(file.readlines()[50])
# 특정 키를 불러 오고 싶으면 index 사용

line = 1
for num in rfile.readlines():
    print(line, num, end='')
    line += 1
# 리눅스의 set nu 하듯이 출력
rfile.close()
# 열린 파일 닫기(작업이 끝나면 항상)

wfile = open('D:\\newfile', 'w', encoding='utf-8')
wfile.write('쓰기 작업을 할 문자열 작성\n')
wfile.write('여러번 반복 사용 가능\n')
wfile.write('쓰기는 자동 개행이 안됨\n')
wfile.close()
# D드라이브에 newfile 생성 확인

wfile = open('D:\\newfile', 'a', encoding='utf-8')
wfile.write('기존에 작성된 내용 마지막에\n')
wfile.write('추가 쓰기 작업을 수행한다\n')
wfile.close()

img_file = open('D:\\dog.jpeg', 'br')
data = img_file.read()
print(data)
# 사진을 바이너리 형식으로 출력

