import sys
import module1
sys.path.append('D:\\AWS(KSH)\\my_modules')
# 같은 위치에 존재하지 않는 모듈은 경로 추가 해주어야 한다

import module2
from dev import module3
from dev.module3 import GLOBAL_VAR
from dev.module3 import 예약어보기
# from 모듈명 import 변수or함수 활용
# 폴더.파일
# 해당 경로가 명시되거나 같은 위치라는 전제가 있어야 한다

print(module1.GLOBAL_VAR)
print(module1.예약어보기())
print(sys.path)
# ['D:\\Aws(ksh)\\Python_Coding', 'D:\\Python38\\python38.zip', 'D:\\Python38\\DLLs', 'D:\\Python38\\lib', 'D:\\Python38', 'D:\\Python38\\lib\\site-packages']
# 0번 인덱스 주소 참조
# 1번 인덱스 주소 참조
# 2번 인덱스 주소 참조
# ~
# 주소 목록에 my_modules가 없다
# sys.path.append로 주소 추가 결과
# ['D:\\Aws(ksh)\\Python_Coding', 'D:\\Python38\\python38.zip', 'D:\\Python38\\DLLs', 'D:\\Python38\\lib', 'D:\\Python38', 'D:\\Python38\\lib\\site-packages', 'D:\\AWS(KSH)\\my_modules']
print(module2.GLOBAL_VAR)
print(module2.예약어보기())
print(module3.GLOBAL_VAR)
print(module3.예약어보기())
print(GLOBAL_VAR)
print(예약어보기())