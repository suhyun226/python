# Class
# 프로그램 코드를 객체화 하여 사용할 수 있게 만든다.
# 객체(사물) = 속성(변수, 애트리뷰트-Attribute) + 기능(함수, 메서드-Method)

class 클래스명():
    속성 = '속성값'

    def 메서드(self):
        return '메서드 실행 결과 반환'
    # self 인자 꼭 필요

ins = 클래스명()
# 인스턴스화(객체화)라고 한다
# 클래스(=도면) -->> 객체(=사물) 
print(ins.속성)
print(ins.메서드())

class Car():
    # 클래스 속성 : 인스턴스는 다르지만 공통된 속성이 필요한 경우
    #              모든 인스턴스가 공유할 수 있는 속성이 필요한 경우
    시동 = False

    def __init__(self, 엔진='8기통', 연료종류='휘발유'):
        # 인스턴스 속성 : 인스턴스 마다 다른 속성이 필요한 경우
        self.엔진 = 엔진
        self.연료종류 = 연료종류
        self.바퀴수 = 4
        self.연료종류 = '디젤'
        self.엔진 = '8기통'
        self.문짝수 = 4
        self.브랜드 = '부가티'
        self.크기 = '중형'
        self.타입 = '스포츠'
        self.속도 = 0

    def 전진(self):
        self.속도 += 10
        print(f'{self.속도}km/s의 속도로 전진 합니다.')

    def 후진(self):
        self.속도 -= 10
        print(f'{self.속도}km/s의 속도로 후진 합니다.')

    def 우회전(self):
        print('우회전 합니다.')

    def 좌회전(self):
        print('좌회전 합니다.')

    def 정지(self):
        self.속도 = 0
        print('정지 합니다.')

    def 정보(self):
        print(f'이 차는 {self. 엔진}을 사용하고, 연료는 {self.연료종류}(을)를 이용합니다.')

print('\n')

my_car = Car('하이브리드', '경유')
my_car.전진()
my_car.좌회전()
my_car.정보()
print(my_car.브랜드)

