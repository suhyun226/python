# root:x:0:0:root:/root:/bin/bash
# 1    2 3 4 5    6     7

# 1 : 계정명
# 2 : 패스워드
# 3 : UID
# 4 : GID
# 5 : 그룹명
# 6 : 홈 디렉터리
# 7 : 쉘 종료

file = open('D:\\Aws(ksh)\\Python_Coding\\passwd', 'r', encoding='utf-8')

data = file.read()
file.close()

# str.split : 문자열 함수로 문자열의 특정 문자를 기준으로 문자열을 나눈다.
#             나눈 문자열을 리스트에 담긴다.
line_data = data.split('\n')
# 개행을 기준으로 분리
word = []
# input(word)
# 하나씩 하나씩 디버깅 용도
for d in line_data:
    t = d.split(':')
    tmp = {
        '계정명': t[0], '패스워드': t[1], 'UID': t[2], 'GID': t[3],
        '그룹명': t[4], '홈디렉터리': t[5], '쉘': t[6]
    }
    word.append(tmp)
    # input(word)
# 리스트 안에 리스트 형식으로 추가

print(word)

print('\n')


def listPasswd(path, dtype='list'):
    file = open(path, 'r', encoding='utf-8')
    data = file.read().split('\n')
    file.close()
    
    if dtype == 'list':
        for idx in range(len(data)):
            data[idx] = data[idx].split(':')
        # return [data[idx].split(':') for idx in range(len(data))]
        # 바로 위 두줄을 한줄로 만듦
    elif dtype == 'dict':
        col_name = ('계정명', '패스워드', 'UID', 'GID', '그룹명', '홈디렉터리', '쉘')
        for idx in range(len(data)):
            data[idx] = dict(zip(col_name, data[idx].split(':')))
        # return [dict(zip(col_name, data[idx].split(':'))) for idx in range(len(data))]

    return data

print(listPasswd('D:\\Aws(ksh)\\Python_Coding\\passwd', dtype='dict'))
# dtype 'list'나 'dict' 중 하나 선택
