import os
import subprocess

def shellCommand(*args):
    cmd = subprocess.Popen(args, shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
    return cmd.communicate()

print(shellCommand('aws'))
