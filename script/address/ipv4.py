import re

def classful(address, *, network=False):
    address = valid(address)
    first_octet = int(address.split('.')[0])
    if 0 <= first_octet <= 127:
        mask = '255.0.0.0'
    elif 128 <= first_octet <= 191:
        mask = '255.255.0.0'
    elif 192 <= first_octet <= 223:
        mask = '255.255.255.0'
    else:
        mask = '255.255.255.255'

    if network:
        addr_mask = list(zip(address.split('.'), mask.split('.')))
        net = []
        for a, m in addr_mask:
            num = int(a) & int(m)
            net.append(str(num))
        address = '.'.join(net)

        # address = '.'.join(map(lambda num: str(int(num[0]) & int(num[1])),
        #     zip(address.split('.'), mask.split('.'))))

    return address, mask

def valid(address):
    com = re.compile('(([0-1]?[0-9]?[0-9]?|2[0-4][0-9]|25[0-5])\\.){3}([0-1]?[0-9]?[0-9]?|2[0-4][0-9]|25[0-5])')
    if not com.match(address):
        raise ValueError(f'"{address}" 잘못된 IPv4 주소 형식입니다.')
    return address

def netmask(address):
    try:
        mask = valid(address)        
    except ValueError:
        raise ValueError(f'"{address}" 잘못된 Netmask 주소 형식입니다.')

    mask_bit = ''.join([bin(int(b))[2:] for b in mask.split('.')])
    chk_bit = [mask_bit[0], 0]
    for idx in range(1, len(mask_bit)):
        if chk_bit[0] != mask_bit[idx]:
            chk_bit[0] = mask_bit[idx]
            chk_bit[1] += 1

        if mask_bit[0] == '0':
            if chk_bit[1] == 1:
                raise ValueError(f'"{address}" 올바르지 않은 서브넷마스크 형식입니다.')

        if chk_bit[1] >= 2:
            raise ValueError(f'"{mask}" 올바르지 않은 서브넷마스크 형식입니다.')
    return mask

    
if __name__ == '__main__':
    print(classful('10.168.1.1'))
    print(classful('172.168.1.1'))
    print(classful('192.168.1.1'))
    print(classful('10.16.1.1', network=True))
    print(classful('172.16.1.1', network=True))
    print(classful('192.168.1.1', network=True))