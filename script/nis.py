#! /usr/bin/python3

import argparse
from netscript_teacher import NetworkInterface

def show(args):
    val = ''
    if args.all:
        interface = NetworkInterface(file='ifcfg-'+args.device)
        for k in interface.net_attrs.keys():
            val += f'{k}={interface.net_attrs.get(k)}\n'
    else:
        # --ip
        if args.ip:
            val += f'{"IPADDR"}={interface.net_attrs.get("IPADDR")}\n'
        # --mask
        if args.mask:
            val += f'{"NETMASK"}={interface.net_attrs.get("NETMASK")}\n'
        # --prefix
        if args.prefix:
            val += f'{"PREFIX"}={interface.net_attrs.get("PREFIX")}\n'
        # --gw
        if args.gw:
            val += f'{"GATEWAY"}={interface.net_attrs.get("GATEWAY")}\n'
        # --dns
        if args.dns:
            val += f'{"DNS1"}={interface.net_attrs.get("DNS1")}\n'
            val += f'{"DNS2"}={interface.net_attrs.get("DNS2")}\n'
            val += f'{"DNS3"}={interface.net_attrs.get("DNS3")}\n'
        # --onboot
        if args.onboot:
            val += f'{"ONBOOT"}={interface.net_attrs.get("ONBOOT")}\n'
        # --defaultroute
        if args.defaultroute:
            val += f'{"DEFROUTE"}={interface.net_attrs.get("DEFROUTE")}\n'
        # --dhcp
        if args.dhcp:
            val += f'{"BOOTPROTO"}={interface.net_attrs.get("BOOTPROTO")}\n'
        
    print(val)

def setting(args):
    interface = NetworkInterface(file='ifcfg-'+args.device)
    if args.ip:
        interface.setAddress(args.ip)

    if args.gw:
        interface.setGateway(args.gw)

    if args.dns:
        interface.setDNS(args.dns[0], *args.dns[1:3])
        # DNS1~3까지 

    if args.dhcp:
        if args.dhcp == 'no':
            interface.disableDHCP()
        elif args.dhcp == 'yes':
            interface.enableDHCP()

    interface.setInterface(file='ifcfg-'+args.device)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(help='네트워크 스크립트를 확인 및 설정')

    show_parser = subparsers.add_parser('show')
    show_parser.add_argument('--all', help='모든 정보 확인', action='store_true')
    show_parser.add_argument('--device', help='네트워크 스크립트를 확인 할 디바이스 명', default='ens33')
    show_parser.add_argument('--ip', help='IP 주소 정보 확인', action='store_true')
    show_parser.add_argument('--mask', help='NETMASK 정보 확인', action='store_true')
    show_parser.add_argument('--prefix', help='PREFIX 정보 확인', action='store_true')
    show_parser.add_argument('--gw', help='Gateway 정보 확인', action='store_true')
    show_parser.add_argument('--dns', help='DNS 정보 확인', action='store_true')
    show_parser.add_argument('--onboot', help='자동 활성화 확인', action='store_true')
    show_parser.add_argument('--defaultroute', help='기본 라우트 활성화 확인', action='store_true')
    show_parser.add_argument('--dhcp', help='DHCP 활성화 확인', action='store_true')
    show_parser.set_defaults(func=show)
    
    setting_parser = subparsers.add_parser('setting')
    setting_parser.add_argument('--device', help='네트워크 스크립트를 확인 할 디바이스 명', default='ens33')
    setting_parser.add_argument('--ip', help='네트워크 스크립트에 구성 할 IP 주소')
    setting_parser.add_argument('--gw', help='네트워크 스크립트에 구성 할 GW 주소')
    setting_parser.add_argument('--dns', help='네트워크 스크립트에 구성 할 DNS 주소', nargs='+', metavar=('dns1', 'dns2'))
    setting_parser.add_argument('--dhcp', help='DHCP 활성화 유무', choices=('yes', 'no'))
    setting_parser.add_argument('--onboot', help='재부팅시 자동 활성화 유무', choices=('yes', 'no'))
    setting_parser.set_defaults(func=setting)

    # args = parser.parse_args(['show', '--device', 'ens34', '--all'])
    # print(args)

    # args = parser.parse_args(['setting', '--ip', '1.1.1.1', '--dhcp', 'yes'])
    # print(args)


    # parser.add_argument(
    #     'device', help='디바이스명을 설정하세요.'
    # )
    # parser.add_argument(
    #     '-s', '--show', help='네트워크 스크립트 정보를 출력합니다.',
    #     action='store_true'
    #     # True 값 저장?
    # )
    # parser.add_argument(
    #     '--ip', help='구성 할 IP 주소를 설정하세요.'
    # )
    # parser.add_argument(
    #     '--gw', help='구성 할 GW 주소를 설정하세요.'
    # )
    # parser.add_argument(
    #     '--mask', help='구성 할 mask 주소를 설정하세요.'
    # )

    args = parser.parse_args()
    # 파싱=파서=맵핑=1대1매칭
    if args == argparse.Namespace():
        print(parser.parse_args(['--help']))
    else:
        args.func(args)
    # print(args.device)
    # print(args.show)
    # if args.show:
    #     show(args.device)
        # show의 action에 True 값 저장하여 device의 정보 볼수 있다


######################################################################
# 터미널 shell창에서 입력
# python3 script/nis.py --help
# python3 script/nis.py --show ens34

# 쉬뱅 사용하고 싶으면 맨위에 설정 후, 파일에 대해서 실행 권한을 주어야 한다
# chmod 731 script/nis.py
# 이후 명령어 실행에서 python3가 필요없다
# script/nis.py --show ens34
# nis.py -> nis로 바꿔도 정상 작동한다

# Enter 개행
# Windows : CRLF
# Linux : LF
# 우측 하단 / 줄 시퀀스의 끝 선택 / (LF) 선택
######################################################################