import os, re, sys, subprocess
# re는 정규표현식 사용 모듈
from address import ipv4

# 객체 = 속성 + 기능
#  기능 : 활성화/비활성화/속성변경
#  속성 : 주소, 마스크, DNS, 활성/비활성상태

class NetworkInterface():
    def __init__(self, path='/etc/sysconfig/network-scripts', file='ifcfg-ens33'):
    # 클래스 인스턴스화 할 때 한번 사용
        self.path = path
        self.file = file
        # self. 을 붙이면 instance 변수(속성)로 되서 인스턴스 내에서 자유롭게 사용가능
        # self. 가 없으면 그냥 지역변수
            # 인스턴스 속성은 같은 클래스 내에서 인스턴스로 구분된다
        self.getInterface(self.path, self.file)
        # '인터페이스 정보' 가져오기 위한 메서드 호출

    def getAddress(self):
        return self.net_attrs.get('IPADDR')

    def setAddress(self, address):
        address = ipv4.valid(address)
        self.net_attrs.update({'IPADDR': address})

    def getPrefix(self):
        prefix = self.net_attrs.get('PREFIX')
        if not prefix:
            netmask = ipv4.netmask(self.net_attrs.get('NETMASK')).split('.')
            prefix = 0
            for octet in netmask:
                prefix += f'{int(octet):b}'.count('1')
        return int(prefix)

    def setPrefix(self, prefix):
        self.net_attrs.update({'PREFIX': str(prefix)})

    def getGateway(self):
        return self.net_attrs.get('GATEWAY')

    def setGateway(self, gateway):
        gateway = ipv4.valid(gateway)
        self.net_attrs.update({'GATEWAY':gateway})
    
    def getDNS(self):
        return self.net_attrs.get('DNS1'), self.net_attrs.get('DNS2'), self.net_attrs.get('DNS3')

    def setDNS(self, dns1, *args):
        dns1 = ipv4.valid(dns1)
        self.net_attrs.update({'DNS1': dns1})

        for idx in range(len(args[:2])):
            dns = ipv4.valid(args[idx])
            self.net_attrs.update({f'DNS{idx+2}': dns})

    def getSubnetmask(self):
        netmask = self.net_attrs.get('NETMASK')
        if not netmask:
            if 0 <= int(self.net_attrs.get('PREFIX')) <= 32:
                netmask = '1' * int(self.net_attrs.get('PREFIX'))
                netmask += '0' * (32 - len(netmask))
                netmask = '.'.join([
                    str(int(netmask[:8], 2)), str(int(netmask[8:16], 2)),
                    str(int(netmask[16:24], 2)), str(int(netmask[24:], 2))
                    # int('100', 2) <- 111을 2진수로 바꿔서 4가 된다
                ])
            else:
                raise ValueError('잘못된 Prefix가 있어서 변환에 실패하였습니다.')
        return netmask

    def enableDHCP(self):
        self.net_attrs.update({'BOOTPROTO': 'dhcp'})
    
    def disableDHCP(self):
        self.net_attrs.update({'BOOTPROTO': 'none'})

    def getInterface(self, path='/etc/sysconfig/network-scripts', file='ifcfg-ens33'):
        self.path, self.file = path, file
        with open(os.path.join(self.path, self.file), 'r', encoding='utf-8') as net_script:
            data = net_script.read()
            data = data.split('\n')

        if data[-1] == '':
            data = data[0:-1]

        self.net_attrs = {}
        for d in data:
            self.net_attrs.update({d.split('=')[0]: d.split('=')[1]})

    def _up(self):
        cmd = subprocess.Popen(
            f'ifup {self.net_attrs.get("DEVICE").lower()}', encoding='utf-8',
            shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE
        )
        res, err = cmd.communicate()
        print(err if res == '' else res, end='')
        self._checkPing()

    def _down(self):
        cmd = subprocess.Popen(
            f'ifdown {self.net_attrs.get("DEVICE").lower()}', encoding='utf-8',
            shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE
        )
        res, err = cmd.communicate()
        print(err if res == '' else res, end='')

    def _checkPing(self):
        cmd = subprocess.Popen(
            f'ping {self.net_attrs.get("GATEWAY")} -c 3', encoding='utf-8',
            shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE
        )
        res, err = cmd.communicate()
        print(err if res == '' else res, end='')

    def setInterface(self, path='/etc/sysconfig/network-scripts', file='ifcfg-ens33'):
        self.path, self.file = path, file

        s = ''
        for k, v in self.net_attrs.items():
            s += f'{k}={v}\n'

        with open(os.path.join(path, file), 'w', encoding='utf-8') as net_script:
            net_script.write(s)

        self._down()
        self._up()

        # cmd = subprocess.Popen(['ifdown', self.net_attrs.get('DEVICE').lower()], shell=True,
        #     stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        # res, err = cmd.communicate()
        # return res if res else err



# def getNetwork(path='/etc/sysconfig/network-scripts',
# file='ifcfg-ens33'):
#     with open(os.path.join(path, file), 'r', encoding='utf-8') as net_script:
#         data = net_script.read()
#         data = data.split('\n')

#     if data[-1] == '':
#         data = data[0:-1]
#     else:
#         data = data

#     tmp = {}
#     for d in data:
#         tmp.update({d.split('=')[0]: d.split('=')[1]})

#     return tmp

# def setNetwork(name, value,
# path='/etc/sysconfig/network-scripts', file='ifcfg-ens33'):
#     data = getNetwork(path, file)
#     data.update({name.upper(): value})
#     s = ''
#     for k, v in data.items():
#         s += f'{k}={v}\n'
#     with open(os.path.join(path, file), 'w', encoding='utf-8') as net_script:
#         net_script.write(s)
#     return s

# 이 스크립트 파일이 메인으로 실행되는 경우 동작할 코드를 작성
if __name__ == '__main__':
    # 모듈 및 패키지화 하여 작성한 코드에 대해 별도의 테스트 코드를
    # 작성하기 위해 사용
    # interface = NetworkInterface(file='ifcfg-ens34')
    # print(interface.getAddress())
    # print(interface.getGateway())
    # print(interface.getPrefix())
    pass
#     args = sys.argv[1:]
    
#     if args[0] == 'show':
#         if args[1] == 'dev':
#             interface = NetworkInterface(file='ifcfg-'+args[2])
#         print(f'IP 주소 : {interface.getAddress()} /{interface.getPrefix()}')
#         print(f'GW 주소 : {interface.getGateway()}')
#         print(f'DNS 주소 : {interface.getDNS()}')
#         print(f'NETMASK : {interface.getSubnetmask()}')
#     elif args[0] == 'set':
#         if args[1] == 'addr':
#             interface.setAddress(args[2])
#         elif args[1] == 'gw':
#             interface.setGateway(args[2])
#         elif args[1] == 'dns':
#             interface.setDNS(args[2])
#         interface.setInterface()