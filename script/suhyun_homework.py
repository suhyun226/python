# Arranging Files in the Download Directory
# 다운로드 받은 파일을 오전 8시 마다 파일 종류를 구분하여 각 폴더에 자동으로 이동.
# jpg, mp3 등등 같은 타입끼리 따로 저장된다
import os
import shutil    ## 파일과 디렉터리 관리 작업 모듈
import datetime    ## 날짜, 시간 조작 클래스 제공 모듈

file_mapping = [
    # ([확장자, 확장자], '디렉터리명')
    (['docx', 'hwp', 'pdf', 'ppt', 'xls'], '문서'),
    (['jpg', 'jpeg', 'gif', 'png'], '사진'),
    (['mp4', 'avi', 'mkv'], '비디오'),
    (['mp3', 'wav', 'flac'], '오디오'),
    (['exe', 'msi'], '프로그램'),
    (['zip', '7z', 'tar', 'gz', 'xz', 'bz'], '압축')
]

def temp(filename, directroy):
    if not os.path.exists(directroy):
        os.mkdir(directroy)
    shutil.move(filename, directroy)

# 파일을 정리할 디렉터리 지정
download_dir = 'C:\\Users\\user\\Downloads'    # input('')

while True:
    #calculating current hour, minute and second
    today = datetime.datetime.today()
    # today = str(today)
    current_hour = f'{today.hour:02}'      # today[11:13]
    current_minute = f'{today.minute:02}'  # today[14:16]
    current_sec = f'{today.second:02}'     # today[17:19]

    # making sure that system will arrange files at 08:00
    # if current_hour == '08' & current_minute == '00' & current_sec == '00':
    if current_hour != '08':

        # changing directory to download
        # os.chdir("path_to_Download_directory")
        os.chdir(download_dir)
        # saving all file names in a list
        files = os.listdir(os.getcwd())

        for filename in files:
            # ignoring directories
            if not os.path.isdir(filename):
                extension = filename.split('.')[-1]

                for exts, folder in file_mapping:
                    if extension.lower() in exts:
                        temp(filename, folder)