# 키: 값 형식으로 원하는 정보를 찾을 수 있도록 함수를 생성
# 새로운 키, 값을 설정하여 파일로 저장할 수 있는 함수 생성
# 기존의 키, 값을 찾아 수정하고 파일로 저장할 수 있는 함수 생성
import os
import sys

def listEns33(path):
    tmp = {}
    file = open(path, 'r', encoding='utf-8')
    data = file.read().split('\n')
    print('@@@', data)
    # print(dir(data))
    file.close    

    if data[-1] == '':
        data = data[0:-1]
    print('---', data)
   
    # print(len(data))
    for idx1 in range(len(data)):
        data[idx1] = data[idx1].split('=')
        # print(data[idx1])     
        # if data[idx1][-1] == '':
        #     data[idx1] = data[idx1][-1].remove('')
        # else: 
        #     pass 
        # print(data[idx1][-1])
        tmp.update({data[idx1][0]: data[idx1][1]})     

        # print(tmp)
        # print(data[idx1])
   
    # print(tmp)
    return tmp

test1 = listEns33('D:\\Aws(ksh)\\Python_Coding\\ifcfg-ens33')
# print(test1)
# ifcfg-ens33의 데이터를 키:값 형식으로 분리

print('\n')


def updateEns33(path, newkey, value):
    data = listEns33(path)
    # print(data)

    data.update({newkey.upper(): value})
    s = ''
    for k, v in data.items():
        s += f'{k}={v}\n'
    with open(os.path.join(path), 'w', encoding='utf-8') as net_script:
        net_script.write(s)
        
    return s
    
     
    


test2 = updateEns33('D:\\Aws(ksh)\\Python_Coding\\ifcfg-ens33', 'ipaddr', '172.16.10.10')
# print(test2)

print('\n')



# # 이 스크립트 파일이 메인으로 실행되는 경우 동작할 코드를 작성
# if __name__ == '__main__':
#     print(sys.argv)
#     args = sys.argv[1:]
# # PS D:\Aws(ksh)\Python_Coding> .\netscript.py -a b c d 
# # ['D:\\Aws(ksh)\\Python_Coding\\netscript.py', '-a', 'b', 'c', 'd']
#     if args[0] == 'get':
#         print(listEns33('D:\\Aws(ksh)\\Python_Coding\\ifcfg-ens33').get(args[1].upper()))