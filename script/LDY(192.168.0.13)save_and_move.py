### https://codereview.stackexchange.com/questions/241644/file-automation-using-python-on-linux
### 파일을 저장하면 해당 파일 위치로 이동하는 코드


#!/usr/bin/env python

import os
import time
import os.path
from random import randint
from ExtraInfo import types, locations, docs, working_directories


class FileOrganizer:

    def __init__(self, directory_path):
        self.directory_path = directory_path

    def path_maker(self, root, file_name):
        """(str, str) -> str

        Returns a string containing the full path of a file,
        from root of the file and its name.

        >>> path_maker("/home/hama/Downloads", "area.cpp")
        "/home/hama/Downloads/area.cpp"
        >>> path_maker("/home/hama/Downloads/", "FuzzBuzz.py")
        "/home/hama/Downloads/FuzzBuzz.py"
        """
        return os.path.join(root, file_name)  # 경로를 이름으로 가지는 파일로 변환한뒤 , 해당 파일의 권한을 root 로 준다.

    def extension_finder(self, path):
        """(str) -> str

        Takes in a string of full path of a file. If exists,
        returns a string of its extension, else returns False. 

        >>> extension_finder("/home/hama/Downloads/area.cpp")
        ".cpp"
        >>> extension_finder("/home/hama/Downloads/FuzzBuzz.py")
        ".py"
        """ 

        if os.path.exists(path):
            if os.path.isfile(path):
                return os.path.splitext(path)[1] # 이름에 파일경로를 담고있는 파일에서, 파일 경로를 따로 뽑아낸다
        return False

    def category_selector(self, extension): # 파일 확장자 명을 기준으로 파일을 분류한다.
        """(str) -> str

        Takes in a string of an extension of a file. If not False,
        returns the category of the extension, else returns False.

        Precondition: The extension must be in one of the categories.

        >>> category_selector(".cpp")
        "programming-files"
        >>> category_selector(".mp4")
        "video"
        """

        if extension != False:
            for category in types:
                if extension in types[category]:
                    return category
                    break
            return False

    def get_prefix(self, path): 
        """(str) -> str

        Takes in a string of full path of a file. If it is one of the doc
        categories returns the first 3 characters of name of the file, else 2.

        Precondition: A prefix of a specific directory should be provided
        at the beginning of the name of the file.

        >>> get_prefix("/home/hama/Downloads/umaMath-week11.pdf")
        "uma"
        >>> get_prefix("/home/hama/Downloads/pyFuzzBuzz.py")
        "py"
        """

        prefix = os.path.basename(path) # 파일 경로를 가진 파일을 카테고리화 했을 때 doc 에 해당하지 않는다면 처리할 방법
        if self.category_selector(self.extension_finder(path)) not in docs:
            return prefix[:2]
        else:
            return prefix[:3]

    def get_original_name(self, path): # 파일명이 경로일 때, 파일 경로부분을 제외한 파일을 순수이름을 따로 분리해내는 코드
        """(str) -> str

        Takes in a string of full path of a file. returns a string of
        the original file name without any prefix.

        Precondition: A prefix of a specific directory should be provided
        at the beginning of the name of the file.

        >>> get_original_name("/home/hama/Downloads/umaMath-week11.pdf")
        "Math-week11.pdf"
        >>> get_original_name("/home/hama/Downloads/pyFuzzBuzz.py")
        "FuzzBuzz.py"
        """

        file_name = os.path.basename(path)
        if self.category_selector(self.extension_finder(path)) not in docs:
            return file_name[2:]
        else:
            return file_name[3:]

    def random_name_generator(self, path): # 파일 생성시 파일이름 뒤에 식별자를 붙여줌
        """(str) -> str

        Takes in a string of full path of a file. Generates a random
        integer at the end of the name of the file, the returns the new name.

        >>> random_name_generator("/home/hama/Downloads/umaMath-week11.pdf")
        "Math-week11.pdf"
        >>> random_name_generator("/home/hama/Downloads/pyFuzzBuzz.py")
        "FuzzBuzz.py"
        """

        file_name = os.path.splitext(path)[0]
        extension = os.path.splitext(path)[1]
        return f"""{file_name}-{randint(1, 250) % randint(1, 250)}{extension}"""

    def copy(self, file_source, destination_root):
        """(str, str) -> str

        Returns a string containing the full path of the newly moved file,
        from a full path of a file and root of the destination.

        Note: If a file with the same name already exists, a new name will be generated.

        >>> copy("/home/hama/Downloads/area.cpp", "/home/hama/Codes/C++/")
        "/home/hama/Codes/C++/area.cpp"
        >>> copy("/home/hama/Downloads/FuzzBuzz.py", "/home/hama/Codes/Python/")
        "/home/hama/Codes/Python/FuzzBuzz.py"
        """

        if not os.path.exists(self.path_maker(destination_root, self.get_original_name(file_source))): # 파일경로로 이동하려고할 때 만약 경로가 존재하지 않는다면 해당 경로를 만들어주는 코드
            file_name = os.path.basename(file_source)
            file_destination = self.path_maker(
                destination_root, self.get_original_name(file_source))
            os.system(f"cp -pa {file_source} {file_destination}")
            return file_destination
        else:
            file_name = self.random_name_generator(self.path_maker(
                destination_root, self.get_original_name(file_source)))
            file_destination = self.path_maker(destination_root, file_name) # 파일 경로를 만들 때 목적지 파일의 이름을 만드는 방식을 지정.
            os.system(f"cp -pa {file_source} {file_destination}")
            return file_destination


# Activated on these directories
paths = [FileOrganizer(f"{directory}") for directory in working_directories]
while True:
    for path in paths:
        # Get the files and directories in the root directory.
        for root, directories, files in os.walk(path.directory_path):
            root, directories, files = root, directories, files
            break

        # List the files in the directory
        list_of_files = []
        for file in files:
            list_of_files.append(path.path_maker(root, file)) # 파일 목록 마지막에 추가한다.

        # Loop through the files and copy each one of them.
        proccess = True
        for file in list_of_files: 
            if proccess:
                current_file = file # 현재 처리하는 작업의 형식이 파일임.

                file_category = path.category_selector( # 
                    path.extension_finder(current_file))
                if file_category in locations:
                    if locations[file_category].get(path.get_prefix(current_file)) != None:
                        destination_root = locations[file_category].get(
                            path.get_prefix(current_file))

                        # Check if there is a whitespace in the path, cause it cause infinite loop.
                        if not (" " in current_file):
                            new_file_destination = path.copy( # 경로를 복제해서 현재 파일에 복사하겠다. 
                                current_file, destination_root)
                        else:
                            continue
                        if os.path.exists(new_file_destination): # 새로운 파일경로에 이미 존재한다면 현재 파일을 삭제하겠다.
                            os.remove(current_file) 

                        # Check if the file is moved and the proccess is done, otherwise wait until it is done.
                        if not os.path.exists(current_file) and os.path.exists(new_file_destination):
                            proccess = True
                        else:
                            proccess = False
                            while not proccess:
                                if not os.path.exists(current_file) and os.path.exists(new_file_destination):
                                    proccess = True
                                else:
                                    proccess = False
                                time.sleep(10)

        time.sleep(5) # 작업후 5초의 간격을 두겠다.

# By: Hama
# Software Engineer to be.